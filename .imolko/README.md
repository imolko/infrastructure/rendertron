# Mantener Sinc repositotio con Github

Añadimos el retetron Github project como el upstream remote:

```
git remote add upstream git@github.com:GoogleChrome/rendertron.git 
```

Ahora se puede extrar desde el originar en caso de que halla algun cambio.

```
git pull upstream main
```

y Finalmente enviamos a gitlab los cambios

```
git push origin main
```

# Aplicar parche tumb-width

Se aplican al momsnto de Construir la imagen un parche para anexar el soporte de width del thumbder.

Ver [.imolko/thumb-width.patch.sh](.imolko/thumb-width.patch.sh)
