import Koa from 'koa';
import sharp from 'sharp';

/**
 * 
 * @param ctx El contexto del request.
 * @param img La imagen generada.
 * @param dimensions Las dimensiones del sreeenshot
 * @returns La misma imagen o la imagen con el tamaño adecuado.
 */
export async function resizeWidthImage(ctx: Koa.Context, width: number) {
    const thumbWidth = Number(ctx.query['thumbWidth']) || 0;
    if (thumbWidth > 0 && thumbWidth < width) {
        const img = await sharp(ctx.body)
            .resize({ width: thumbWidth })
            .toBuffer();

        ctx.set('Content-Type', 'image/jpeg');
        ctx.set('Content-Length', img.length.toString());
        ctx.body = img;
    }
}
