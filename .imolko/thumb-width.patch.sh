#!/bin/bash

MY_PATH=$(dirname "$0")            # relative
MY_PATH=$(cd "$MY_PATH" && pwd)    # absolutized and normalized
if [[ -z "$MY_PATH" ]] ; then
  # error; for some reason, the path is not accessible
  # to the script (e.g. permissions re-evaled after suid)
  exit 1  # fail
fi

if ! grep -q 'ctx.body = img;' "./src/rendertron.ts"
then
    echo "No se encuentra el placeholder 'ctx.body = img;' en ./src/rendertron.ts" >&2
    exit 1
fi

set -e

# Importamos el thumb-wifth.imolko.ts
sed -r -f "${MY_PATH}/thumb-width.sed" ./src/rendertron.ts > ./src/rendertron.new.ts

# Remplazamos el archivo
mv ./src/rendertron.new.ts ./src/rendertron.ts

# Copiamos el parche
cp ./.imolko/thumb-width.imolko.ts ./src/

# Instalamos Las dependencias.
npm install
npm install --save sharp 
npm install --save-dev @types/sharp

# Compilamos El build
npm run build

# Copiamos package to la carpeta build.
cp package*.json build/

# Construimos para produccion.
cd build
npm install --production
cd ..

# Eliminamos npm cache
npm cache clean --force
rm -rf node_modules
